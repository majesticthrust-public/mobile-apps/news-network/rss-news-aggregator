from cx_Freeze import setup, Executable
import sys

# Dependencies are automatically detected, but it might need
# fine tuning.
buildOptions = dict(packages=[], excludes=["tkinter"])

base = "Console"

executables = [Executable("main.py", base=base, targetName="aggregator")]

setup(
    name="rss-news-aggregator",
    version="1.0",
    description="Aggregate Google News RSS feeds and save articles to the database",
    options=dict(build_exe=buildOptions),
    executables=executables,
)
