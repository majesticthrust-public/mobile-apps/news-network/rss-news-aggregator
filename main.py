import asyncio
import aiohttp
import rx
import rx.operators as rxop
import sys
import argparse

from aggregator.feed_aggregator.aio_feedparser import AIOFeedparser
from aggregator.feed_aggregator.aio_newspaper import AIONewspaper

# TODO advance from mvp
# from aggregator.feed_aggregator.feed_aggregator import NewsGatherer, google_news_topics
# from aggregator.feed_aggregator.mvp_feed_aggregator import (
#     NewsGatherer,
#     google_news_topics,
# )
from aggregator.feed_aggregator.sync_feed_aggregator import (
    google_news_topics,
    get_feed_events,
    process_event,
)

from aggregator.config.config import Config

# from aggregator.db.news import NewsDB
from aggregator.db.news_sync import NewsDB


agrparser = argparse.ArgumentParser(
    description="Parse Google News RSS and collect articles into the database"
)
agrparser.add_argument("--config", "-c", type=str, help="Config path")


async def bootstrap(args):
    # loop = asyncio.get_event_loop()

    # set aiohttp ClientSession to multiple singleton consumers
    # timeout = aiohttp.ClientTimeout(total=60)
    # session = aiohttp.ClientSession(timeout=timeout)
    # AIOFeedparser().set_aiohttp_session(session)
    # AIONewspaper().set_aiohttp_session(session)

    # load configuraton
    c = Config()
    c.load_config_cascade(
        [args.config, "/usr/local/news-network-system/config.yaml",]
    )

    # init news database
    # await NewsDB().init(loop=loop)
    NewsDB().init()

    await main(args)
    print("Main done")

    # ns = AIONewspaper()
    # print(
    #     f"Newspaper requests:\n\ttotal: {ns.get_article_total}\n\tcomplete: {ns.get_article_complete}\n\tincomplete: {ns.get_article_incomplete}\n\tsuccessful: {ns.get_article_successful}\n\terrors: {ns.get_article_errors}"
    # )

    NewsDB().close()

    # await asyncio.gather(
    #     # close aiohttp session
    #     # session.close(),
    #     # shut down NewsDB
    #     # NewsDB().close(),
    # )
    print("Finalize done")


# async def main(args):
#     loop = asyncio.get_event_loop()

#     gatherer = NewsGatherer(loop)

#     locales = Config().enabled_locales
#     for locale in locales:
#         for topic in google_news_topics:
#             gatherer.queue_feed(locale, topic)

#     newsDB = NewsDB()

#     # start up the parser and listen to new events
#     gatherer.start()

#     pipe = gatherer.events().pipe(rxop.map(newsDB.add_event))

#     await pipe


async def main(args):
    locales = Config().enabled_locales

    for locale in locales:
        for topic in google_news_topics:
            events = get_feed_events(locale, topic)
            for event in events:
                event = process_event(event)
                NewsDB().add_event(event)

    # events = get_feed_events("ru-RU", "HEALTH")
    # for event in events:
    #     event = process_event(event)
    #     NewsDB().add_event(event)


if __name__ == "__main__":
    import time

    start = time.time()

    args = agrparser.parse_args()
    asyncio.run(bootstrap(args), debug=False)

    end = time.time()

    print(f"Finished in {end - start:.3f}s")
