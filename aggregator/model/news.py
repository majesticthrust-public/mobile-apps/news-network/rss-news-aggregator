from typing import List, Optional
from dataclasses import dataclass
from datetime import datetime


@dataclass
class NewsArticle:
    title: str
    text: str
    authors: List[str]
    top_image: str
    images: List[str]
    videos: List[str]
    publish_date: Optional[datetime]
    source_url: str
    source_name: str
    hash_sha256: str


@dataclass
class NewsEvent:
    topic: str
    locale: str
    title: str
    hash_sha256: str
    articles: List[NewsArticle]
