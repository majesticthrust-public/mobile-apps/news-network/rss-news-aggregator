from typing import List, Dict
from pathlib import Path
import json
import asyncio
import aiosqlite
import datetime
from contextlib import asynccontextmanager
from aggregator.common.singleton import Singleton
from aggregator.config.config import Config
from aggregator.model.news import NewsArticle, NewsEvent


class InvalidLocaleException(Exception):
    pass


class NewsDB(metaclass=Singleton):
    """
    Interface to the news database.
    The database stores ell news events and all articles related to these events.
    """

    # TODO cleanup articles with empty text
    # TODO cleanup articles with incorrect text (e.g. are you a robot?) (select from where like?)
    # TODO save news that the newspaper couldn't parse; these news just won't be uploaded to our sites

    _root_folder: Path
    _enabled_locales: List[str]

    # only one writer at a time
    # sqlite doesn't support multiple concurrent writers even in WAL mode
    _writer_connection_locks: Dict[str, asyncio.Lock]

    _news_event_queue: asyncio.Queue

    _loop: asyncio.AbstractEventLoop
    _unfinished_tasks: List[asyncio.Task] = []

    async def init(self, loop: asyncio.AbstractEventLoop):
        """
        Initialize database.

        Arguments:
            - max_connections - max connections per database; connections that go \
                over the counter will block current coroutine
            - loop - asyncio event loop
        """
        self._loop = loop

        config = Config()
        self._enabled_locales = config.enabled_locales
        self._root_folder = config.news_db_folder_path

        # create locks to limit writer connections
        self._writer_connection_locks = {}
        for locale in self._enabled_locales:
            self._writer_connection_locks[locale] = asyncio.Lock()

        # setup news events queue
        self._news_event_queue = asyncio.Queue()
        self._unfinished_tasks.append(
            self._loop.create_task(self._news_events_queue_worker())
        )

        # ensure the folder exists
        self._root_folder.mkdir(parents=True, exist_ok=True)

        # create tables if they don't exist
        init_futures = [self._init_db(locale) for locale in self._enabled_locales]

        # wait for all inits to finish
        return await asyncio.gather(*init_futures, loop=self._loop)

    def add_event(self, event: NewsEvent):
        # asyncio.create_task(self._news_event_queue.put(event))
        self._news_event_queue.put_nowait(event)

    async def _add_event(self, con: aiosqlite.Connection, event: NewsEvent):
        """
        Add `NewsEvent` with all associated articles and topic to the database.
        """
        async with con.cursor() as cur:
            # async with con.cursor() as cur:
            # insert topic
            await cur.execute(
                """
                select id, topic
                from Topics
                where topic=?
                """,
                (event.topic,),
            )
            r = await cur.fetchone()

            # check if topic exists, insert if it doesn't
            if r:
                topic_id, _ = r
            else:
                await cur.execute(
                    """
                    INSERT OR IGNORE INTO Topics (topic)
                    VALUES (?)
                    """,
                    (event.topic,),
                )
                # if there is an integer primary key in the table,
                # rowid is an alias to that primary key
                topic_id = cur.lastrowid

            # check if event exists
            await cur.execute(
                """
                select id, title
                from Events
                where hash_sha256=?
                """,
                (event.hash_sha256,),
            )

            event_id = None
            rows = await cur.fetchall()
            for row in rows:
                e_id, title = row
                if event.title == title:
                    event_id = e_id
                    break

            # if event doesn't exist, insert it
            if event_id is None:
                await cur.execute(
                    """
                    insert or ignore into Events (title, hash_sha256)
                    values (?, ?)
                    """,
                    (event.title, event.hash_sha256),
                )
                event_id = cur.lastrowid

            # insert event-topic junction
            await cur.execute(
                """
                insert or ignore into EventsTopicsJunction (event_id, topic_id)
                values (?, ?)
                """,
                (event_id, topic_id),
            )

            # insert news articles
            for article in event.articles:
                # check if article exists
                # TODO compare more attributes?
                await cur.execute(
                    """
                    select title, text, source_url
                    from News
                    where hash_sha256=?
                    """,
                    (article.hash_sha256,),
                )

                article_exists = False
                rows = await cur.fetchall()
                for row in rows:
                    title, text, source_url = row
                    if (
                        article.title == title and article.text == text
                    ) or article.source_url == source_url:
                        article_exists = True
                        break

                if article_exists:
                    # skip article if exists
                    continue
                else:
                    # insert article
                    await cur.execute(
                        """
                        insert or ignore into News (
                            title,
                            text,
                            authors_original,
                            top_image,
                            images,
                            videos,
                            source_url,
                            publish_date,
                            hash_sha256,
                            event_id
                        ) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
                        """,
                        (
                            article.title,
                            article.text,
                            json.dumps(article.authors, ensure_ascii=False),
                            article.top_image,
                            json.dumps(article.images, ensure_ascii=False),
                            json.dumps(article.videos, ensure_ascii=False),
                            article.source_url,
                            article.publish_date.isoformat()
                            if article.publish_date
                            else datetime.datetime.now().isoformat(),
                            article.hash_sha256,
                            event_id,
                        ),
                    )

    async def _news_events_queue_worker(self):
        """
        A worker that processes news events from a queue.
        """
        # create shared connections
        # uncommitted changes are visible only within a single connection
        # committing is slow, so we work with pending changes moslty
        # and commit once in the end
        connections: Dict[str, aiosqlite.Connection] = {
            locale: await self._create_connection(locale)
            for locale in self._enabled_locales
        }

        while True:
            event: NewsEvent = await self._news_event_queue.get()
            if event is None:
                print("Got None from queue")
                break
            # else:
                print(f"Got event from queue: {event.title}")

            # TODO commit once in a while?

            con = connections[event.locale]
            await self._add_event(con, event)
            self._news_event_queue.task_done()

            print(f"Added event to db: {event.title}")

        for locale, connection in connections.items():
            await connection.commit()
            await connection.close()
            print(f"Closed db connection for {locale}")

    async def _init_db(self, locale):
        """Initialize database with tables and pragmas."""
        async with self._create_connection(locale) as con:
            script = """
            PRAGMA journal_mode=WAL;

            create table if not exists Events (
                id integer primary key AUTOINCREMENT,
                title text not NULL,
                hash_sha256 text not NULL
            );

            create table if not exists Sites (
                id integer PRIMARY key AUTOINCREMENT,
                unique_name text UNIQUE not NULL,
                site_url text not NULL
            );

            create table if not EXISTS News (
                id INTEGER PRIMARY key AUTOINCREMENT,
                title TEXT NOT NULL,
                text TEXT NOT NULL,
                authors_original TEXT,
                authors_cleaned text default "[]",
                top_image text,
                images text,
                videos text,
                source_url text NOT NULL,
                publish_date text not NULL,
                hash_sha256 text not NULL,
                event_id integer not NULL,
                FOREIGN key (event_id) references Events(id)
            );

            create table if not exists Topics (
                id integer PRIMARY key AUTOINCREMENT,
                topic text UNIQUE not NULL 
            );

            create table if not exists NewsSitesJunction (
                article_id integer,
                site_id integer,
                post_url text not NULL,
                primary key (article_id, site_id),
                FOREIGN key (article_id) REFERENCES News(id),
                FOREIGN key (site_id) REFERENCES Sites(id)
            );

            CREATE table if not EXISTS SitesTopicsJunction (
                site_id integer,
                topic_id integer,
                PRIMARY key (site_id, topic_id),
                FOREIGN key (site_id) references Sites(id),
                FOREIGN key (topic_id) REFERENCES Topics(id)
            );

            CREATE table if not EXISTS EventsTopicsJunction (
                event_id integer,
                topic_id integer,
                PRIMARY key (event_id, topic_id),
                FOREIGN key (event_id) references Events(id),
                FOREIGN key (topic_id) REFERENCES Topics(id)
            );
            """
            await con.executescript(script)
            await con.commit()

    def _create_connection(self, locale: str) -> aiosqlite.Connection:
        """
        Acquire database connection for a single transaction.
        Performs check for correct locale.
        """
        if locale not in self._enabled_locales:
            raise InvalidLocaleException(
                f"Database for locale `{locale}` does not exist!"
            )

        path = self._root_folder / f"{locale}.sqlite3"
        return aiosqlite.connect(path, loop=self._loop)

    async def close(self):
        """Shut down all workers, clean up resources."""
        # signal end to news event queue
        self._news_event_queue.put_nowait(None)

        # wait for all tasks to complete
        await asyncio.wait(self._unfinished_tasks)