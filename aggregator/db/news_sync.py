from typing import List, Dict
from pathlib import Path
import json
import sqlite3
import datetime
from aggregator.common.singleton import Singleton
from aggregator.config.config import Config
from aggregator.model.news import NewsEvent


class InvalidLocaleException(Exception):
    pass


class NewsDB(metaclass=Singleton):
    """
    Interface to the news database.
    The database stores ell news events and all articles related to these events.
    """

    # TODO cleanup articles with empty text
    # TODO cleanup articles with incorrect text (e.g. are you a robot?) (select from where like?)
    # TODO save news that the newspaper couldn't parse; these news just won't be uploaded to our sites

    _root_folder: Path
    _enabled_locales: List[str]

    _connections: Dict[str, sqlite3.Connection]

    def init(self):
        """
        Initialize database.
        """

        config = Config()
        self._enabled_locales = config.enabled_locales
        self._root_folder = config.news_db_folder_path

        # ensure the folder exists
        self._root_folder.mkdir(parents=True, exist_ok=True)

        # create connections
        self._connections = {}
        for locale in self._enabled_locales:
            self._connections[locale] = self._create_connection(locale)

        # init tables
        for locale in self._enabled_locales:
            self._init_db(locale)

    def add_event(self, event: NewsEvent):
        """
        Add `NewsEvent` with all associated articles and topic to the database.
        """

        print(f"DB: adding an event with {len(event.articles)} articles")

        con = self._connections[event.locale]
        cur = con.cursor()

        # insert topic
        cur.execute(
            """
            select id, topic
            from Topics
            where topic=?
            """,
            (event.topic,),
        )
        r = cur.fetchone()

        # check if topic exists, insert if it doesn't
        if r:
            topic_id, _ = r
        else:
            cur.execute(
                """
                INSERT OR IGNORE INTO Topics (topic)
                VALUES (?)
                """,
                (event.topic,),
            )
            # if there is an integer primary key in the table,
            # rowid is an alias to that primary key
            topic_id = cur.lastrowid

        # check if event exists
        cur.execute(
            """
            select id, title
            from Events
            where hash_sha256=?
            """,
            (event.hash_sha256,),
        )

        event_id = None
        rows = cur.fetchall()
        for row in rows:
            e_id, title = row
            if event.title == title:
                event_id = e_id
                break

        # if event doesn't exist, insert it
        if event_id is None:
            cur.execute(
                """
                insert or ignore into Events (title, hash_sha256)
                values (?, ?)
                """,
                (event.title, event.hash_sha256),
            )
            event_id = cur.lastrowid

        # insert event-topic junction
        cur.execute(
            """
            insert or ignore into EventsTopicsJunction (event_id, topic_id)
            values (?, ?)
            """,
            (event_id, topic_id),
        )

        # insert news articles
        for article in event.articles:
            # check if article exists
            # TODO compare more attributes?
            cur.execute(
                """
                select title, text, source_url
                from News
                where hash_sha256=?
                """,
                (article.hash_sha256,),
            )

            article_exists = False
            rows = cur.fetchall()
            for row in rows:
                title, text, source_url = row
                if (
                    article.title == title and article.text == text
                ) or article.source_url == source_url:
                    article_exists = True
                    break

            if article_exists:
                # skip article if exists
                continue
            else:
                # insert article
                cur.execute(
                    """
                    insert or ignore into News (
                        title,
                        text,
                        authors_original,
                        top_image,
                        images,
                        videos,
                        source_url,
                        publish_date,
                        hash_sha256,
                        event_id
                    ) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
                    """,
                    (
                        article.title,
                        article.text,
                        json.dumps(article.authors, ensure_ascii=False),
                        article.top_image,
                        json.dumps(article.images, ensure_ascii=False),
                        json.dumps(article.videos, ensure_ascii=False),
                        article.source_url,
                        article.publish_date.isoformat()
                        if article.publish_date
                        else datetime.datetime.now().isoformat(),
                        article.hash_sha256,
                        event_id,
                    ),
                )

        # con.commit()

    def _init_db(self, locale):
        """Initialize database with tables and pragmas."""
        con = self._connections[locale]
        script = """
        PRAGMA journal_mode=WAL;

        create table if not exists Events (
            id integer primary key AUTOINCREMENT,
            title text not NULL,
            hash_sha256 text not NULL
        );

        create table if not exists Sites (
            id integer PRIMARY key AUTOINCREMENT,
            unique_name text UNIQUE not NULL,
            site_url text not NULL
        );

        create table if not EXISTS News (
            id INTEGER PRIMARY key AUTOINCREMENT,
            title TEXT NOT NULL,
            text TEXT NOT NULL,
            authors_original TEXT,
            authors_cleaned text default "[]",
            top_image text,
            images text,
            videos text,
            source_url text NOT NULL,
            publish_date text not NULL,
            hash_sha256 text not NULL,
            event_id integer not NULL,
            FOREIGN key (event_id) references Events(id)
        );

        create table if not exists Topics (
            id integer PRIMARY key AUTOINCREMENT,
            topic text UNIQUE not NULL 
        );

        create table if not exists NewsSitesJunction (
            article_id integer,
            site_id integer,
            post_url text not NULL,
            primary key (article_id, site_id),
            FOREIGN key (article_id) REFERENCES News(id),
            FOREIGN key (site_id) REFERENCES Sites(id)
        );

        CREATE table if not EXISTS SitesTopicsJunction (
            site_id integer,
            topic_id integer,
            PRIMARY key (site_id, topic_id),
            FOREIGN key (site_id) references Sites(id),
            FOREIGN key (topic_id) REFERENCES Topics(id)
        );

        CREATE table if not EXISTS EventsTopicsJunction (
            event_id integer,
            topic_id integer,
            PRIMARY key (event_id, topic_id),
            FOREIGN key (event_id) references Events(id),
            FOREIGN key (topic_id) REFERENCES Topics(id)
        );
        """
        con.executescript(script)
        con.commit()

    def _create_connection(self, locale: str) -> sqlite3.Connection:
        """
        Acquire database connection for a single transaction.
        Performs check for correct locale.
        """
        if locale not in self._enabled_locales:
            raise InvalidLocaleException(
                f"Database for locale `{locale}` does not exist!"
            )

        path = self._root_folder / f"{locale}.sqlite3"
        return sqlite3.connect(path)

    def close(self):
        """Close connections, clean up resources."""
        for connection in self._connections.values():
            connection.commit()
            connection.close()
