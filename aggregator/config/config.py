from typing import List, Union
from aggregator.common.singleton import Singleton
from dataclasses import dataclass
from pathlib import Path
import yaml


class ConfigAlreadyLoadedException(Exception):
    pass


class ConfigDoesNotExistException(Exception):
    pass


class BadConfigException(Exception):
    pass


@dataclass(init=False)
class Config(metaclass=Singleton):
    """
    Config singleton. Provides whole app configuration.

    Config file is assumed to be in a yaml format.

    Sample yaml config:
    ```yaml
    news_db_folder: ../relative/path/to/news_db_folder
    locales:  # list of ISO 639-1 + ISO 3166 alpha 2 codes
        - en-US
        - ru-RU
    ```

    Paths must be either absolute or relative to the config file.
    """

    # path to the config
    config_path: Path = None

    # path to the folder with news database files
    news_db_folder_path: Path

    # enabled locales
    enabled_locales: List[str]

    def load_config_cascade(self, config_paths: List[Union[str, Path]]):
        """Try to load config from one of these paths, listed in the order of trying"""

        for path in config_paths:
            if path is None:
                continue

            path = Path(path)

            if path.exists() and path.is_file():
                try:
                    self.load_config(path)
                    # successfully loaded, can return
                    return
                    
                except ConfigDoesNotExistException:
                    print(f"Config at '{path}' does not exist, trying next...")
                except BadConfigException:
                    print(f"Config at '{path}' is malformed, trying next...")
            else:
                print(f"Path '{path}' does not exist or is not a file, trying next...")

        raise ConfigDoesNotExistException(
            "Could not find config at the following paths:\n"
            + "\n".join(map(str, config_paths))
        )

    def load_config(self, config_path: Union[str, Path]):
        """Load config from the path."""
        if self.config_path is not None:
            raise ConfigAlreadyLoadedException("Configuration file already loaded!")

        config_path = Path(config_path).resolve()

        if not config_path.exists():
            raise ConfigDoesNotExistException(
                f"Config at '{config_path}' does not exist!"
            )
        else:
            try:
                with open(config_path) as f:
                    fcfg = yaml.safe_load(f)
                    self._process_config(config_path, fcfg)
                    self.config_path = config_path

            except yaml.YAMLError as e:
                print(f"Encountered error while parsing config: {e}")
                raise BadConfigException(f"Config at '{config_path}' is malformed!")

    def _process_config(self, config_path: Path, yaml_cfg: dict):
        """
        Set object fields to values from yaml_cfg.
        """

        def rel(p: str):
            """Join config path and relative/absolute path from this config"""
            p = Path(p)
            if p.is_absolute():
                return p
            else:
                return (config_path.parent / p).resolve()

        # TODO process missing/incorrect values, maybe raise some exceptions
        self.news_db_folder_path = rel(yaml_cfg["news_db_folder"])
        self.enabled_locales = yaml_cfg["locales"]
