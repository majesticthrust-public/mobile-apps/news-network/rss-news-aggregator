import aiohttp
from aggregator.common.singleton import Singleton


class AIOHttpClientUser(metaclass=Singleton):
    """
    An ancestor for any class that relies on aiohttp to make requests.
    Generally ClientSession should be shared across the whole application.
    
    This class aims to ease of sharing the session. It assumes that all
    consumers are singletons.
    
    All consumers must inherit from this class, and all consumers must be
    assigned the same session during bootstrap.
    """

    _aiohttp_session: aiohttp.ClientSession

    def set_aiohttp_session(self, session: aiohttp.ClientSession):
        """
        Use provided aiohttp ClientSession when making requests.
        """

        self._aiohttp_session = session
