import feedparser
from aggregator.common.aiohttp_client_user import AIOHttpClientUser


class AIOFeedparser(AIOHttpClientUser):
    async def parse(self, url: str):
        try:
            async with self._aiohttp_session.get(url) as response:
                if response.status != 200:
                    # TODO change to custom exception
                    raise Exception(
                        f"Unsuccessful request; status {response.status}, message: '{await response.text()}'"
                    )

                content = await response.read()
                return feedparser.parse(content)
        except Exception as e:
            print(f"Caught exception while getting feed: {e}")
