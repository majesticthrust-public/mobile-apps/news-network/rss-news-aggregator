from typing import List, Dict, Tuple, Any, NamedTuple
import dataclasses as ds
import hashlib
import asyncio
import rx
import rx.operators as rxop
from rx.scheduler.eventloop import AsyncIOScheduler
from aggregator.feed_aggregator.aio_feedparser import AIOFeedparser
from aggregator.feed_aggregator.aio_newspaper import AIONewspaper
from aggregator.model.news import NewsArticle, NewsEvent


class _FeedRequest(NamedTuple):
    """
    Attributes:
        locale - language and country code pair (`en-US`),\
                ISO 639-1 + ISO 3166 alpha 2
        language - ISO 639-1 language code
        country - ISO 3166 alpha 2 country code
        topic - one of topic accepted by Google News; topic == TOP\
            equivalent to watching top news without topic
    """

    locale: str
    language: str
    country: str
    topic: str


@ds.dataclass
class _NewsArticleProcItem:
    """
    News Article for use in the internal processing pipeline
    """

    event_id: int
    url: str
    source: str
    feed_request: _FeedRequest
    # determines whether the article will be used to name the event
    is_primary: bool = False
    # None during processing, Article after parsing
    article: NewsArticle = None


@ds.dataclass
class _NewsEventProcItem:
    """
    News Event for use in the internal processing pipeline
    """

    id: int
    hash_sha256: str
    feed_request: _FeedRequest
    expected_article_count: int
    processed_articles: int = 0
    articles: List[_NewsArticleProcItem] = ds.field(default_factory=list)


google_news_topics = [
    "TOP",  # special topic, it's not passed as a parameter in url
    "WORLD",
    "NATION",
    "BUSINESS",
    "TECHNOLOGY",
    "ENTERTAINMENT",
    "SPORTS",
    "SCIENCE",
    "HEALTH",
]


class NewsGatherer:
    """
    Collects news from Google RSS Feeds for a given locale and topic.
    Asynchronous.

    The processing pipeline goes like this:
    1. User requests a few feeds by topic and locale
    2. Each request gets converted to a feed pulled from Google News
    3. Each feed gets broken down into Events - a groups of news Articles;\
        each event is saved in internal storage
    4. Each Event gets broken down into Articles;
    5. Each article is passed to it's respective domain pipeline; they are\
        parsed into text and links
    6. Parsing results are joined with the respective Events; Events are\
        emitted upon completion
    """

    _last_event_id: int

    processed_articles: int
    skipped_articles: int

    _aio_loop: asyncio.AbstractEventLoop
    _scheduler: AsyncIOScheduler

    _feed_queue: List[_FeedRequest]
    _feed_request: rx.subject.ReplaySubject
    _is_running: bool

    _unprocessed_events: Dict[int, _NewsEventProcItem]
    _source_processor_subjects: Dict[str, rx.subject.Subject]

    _article_output: rx.subject.Subject
    _event_output: rx.subject.ReplaySubject

    _feedparser: AIOFeedparser
    _newspaper: AIONewspaper

    def __init__(self, loop: asyncio.AbstractEventLoop):
        self._aio_loop = loop
        self._scheduler = AsyncIOScheduler(loop)

        # used to assign unique ids to events
        self._last_event_id = 0

        self.processed_articles = 0
        self.skipped_articles = 0

        self._feed_request = rx.subject.ReplaySubject(1)
        self._feed_queue = []
        self._is_running = False

        self._unprocessed_events = {}
        self._source_processor_subjects = {}

        self._article_output = rx.subject.Subject()
        self._event_output = rx.subject.ReplaySubject()

        self._feedparser = AIOFeedparser()
        self._newspaper = AIONewspaper()

        self._feed_request.pipe(
            # map feed requests to feeds
            # TODO handle potential error when getting feed
            rxop.flat_map(
                lambda rq: rx.from_future(
                    self._aio_loop.create_task(self._get_feed(rq))
                )
            ),
            # map feed to unprocessed events and articles
            # rxop.flat_map(
            #     lambda feed, rq: self._aio_loop.create_task(
            #         self._breakdown_feed(feed, rq)
            #     )
            # )
            rxop.map(lambda feed_rq: self._breakdown_feed(feed_rq[0], feed_rq[1])),
        ).subscribe(
            lambda e_a: self.process_events_articles(e_a[0], e_a[1]),
            scheduler=self._scheduler,
        )

        # group articles, emit built events
        self._article_output.subscribe(self.finalize_article, scheduler=self._scheduler)

    def queue_feed(self, locale: str, topic: str):
        """
        Queue a feed to parse. Does not start parsing immideately.
        Call "run" to start parsing. After launching, queue_feed will raise
        an exception.
        """
        if self._is_running:
            # TODO change to custom excetion
            raise Exception("Can't queue feed: parser is running!")

        language, country = locale.split("-")
        request = _FeedRequest(locale, language, country, topic)
        self._feed_queue.append(request)

    def start(self):
        """
        Start parsing.
        """
        if self._is_running:
            # TODO change to custom excetion
            raise Exception("Can't run: parser is already running!")

        for request in self._feed_queue:
            self._feed_request.on_next(request)

        self._feed_request.on_completed()

    def events(self) -> rx.Observable:
        """
        Returns an observable that emits parsed news events.
        Completes when all events are parsed successfully.
        """
        return self._event_output

    async def _get_feed(self, request: _FeedRequest) -> Tuple[_FeedRequest, Any]:
        """
        Arguments:
            language - ISO 639-1 language code
            country - ISO 3166 alpha 2 country code
            topic - one of topic accepted by Google News; topic == TOP\
                equivalent to watching top news without topic
        """

        _, language, country, topic = request

        # print(f"Getting feed {language} {country} {topic}")

        if topic == "TOP":
            url = f"https://news.google.com/news/rss?hl={language}&gl={country}"
        elif topic in google_news_topics:
            url = (
                "https://news.google.com/news/rss"
                f"/headlines/section/topic/{topic}"
                f"?hl={language}&gl={country}"
            )
        else:
            raise Exception("Invalid topic {}".format(topic))

        feed = await self._feedparser.parse(url)

        # print(f"Got feed {language} {country} {topic}, {len(feed.entries)} entries")
        return feed, request

    def _breakdown_feed(
        self, feed, request: _FeedRequest
    ) -> Tuple[List[_NewsEventProcItem], List[_NewsArticleProcItem]]:
        """
        Breakdowns the feed into unprocessed Events and Articles.
        Articles are linked to Events via event_id's.
        Returns both Events and Articles.
        """
        # print(
        #     f"Breaking down feed {request.language} {request.country} {request.topic}"
        # )
        events = []
        articles = []
        for entry in feed.entries:
            event_id = self._generate_event_id()

            m = hashlib.sha256()
            m.update(entry.title.encode())

            event = _NewsEventProcItem(
                id=event_id,
                hash_sha256=m.hexdigest(),
                feed_request=request,
                # primary article + articles parsed from description
                # TODO add count when parsing description
                expected_article_count=1,
            )

            events.append(event)

            articles.append(
                _NewsArticleProcItem(
                    event_id=event_id,
                    feed_request=request,
                    is_primary=True,
                    url=entry.link,
                    source=entry.source.title,
                )
            )

            # TODO parse event description, extract other articles

        # print(
        #     f"Feed {request.language} {request.country} {request.topic} yielded {len(events)} events and {len(articles)} articles"
        # )

        return events, articles

    def _distribute_unprocessed_articles(
        self, articles: List[_NewsArticleProcItem]
    ) -> Dict[str, List[_NewsArticleProcItem]]:
        """
        Distributes unprocessed articles by source.
        Returns a dict containing lists of unprocessed articles\
            grouped by source.
        """
        article_groups = {}
        for article in articles:
            group = article_groups.get(article.source, [])
            group.append(article)
            article_groups[article.source] = group

        return article_groups

    async def _parse_article(
        self, unprocessed_article: _NewsArticleProcItem
    ) -> _NewsArticleProcItem:
        """
        Downloads and parses an article.

        Arguments:
            url - url to a news post
            language - ISO-639-1 language code, as accepted by Newspaper3k

        Returns either a parsed article, or None, if parsing is unsuccessful.
        """
        # print(f"Parsing article for event {unprocessed_article.event_id}")

        url = unprocessed_article.url
        language = unprocessed_article.feed_request.language
        article = await self._newspaper.get_article(url, language=language)

        if article is None:
            print("Could not parse article")
            return unprocessed_article

        # cleanup text
        article.title = article.title.strip()
        article.text = article.text.strip()

        # skip malformed article
        if len(article.title) == 0 or len(article.text) == 0:
            # TODO?
            pass

        try:
            # generate hash for article fingerprinting
            m = hashlib.sha256()
            m.update(article.title.encode())
            m.update(article.text.encode())
            hash_sha256 = m.hexdigest()
        except Exception as e:
            print(f"Caught exception while generating article hash: {e}")
            return unprocessed_article

        try:
            unprocessed_article.article = NewsArticle(
                title=article.title,
                text=article.text,
                authors=article.authors,
                top_image=article.top_image,
                images=list(article.images),
                videos=list(article.movies),
                publish_date=article.publish_date or None,
                source_url=article.url,
                source_name=unprocessed_article.source,
                hash_sha256=hash_sha256,
            )
            print(f"Got article: {article.title}")
        except Exception as e:
            print(f"Caught exception while processing article: {e}")
            unprocessed_article.article = None

        # print(f"Done parsing article for event {unprocessed_article.event_id}")
        return unprocessed_article

    def process_events_articles(
        self, events: List[_NewsEventProcItem], articles: List[_NewsArticleProcItem]
    ):
        """
        Saves events to internal dict storage by id.
        Sends each article into it's own source's processor subject.
        Creates article source subject if it doesn't exist.
        """
        # print(f"Got to process {len(events)} events and {len(articles)} articles")
        # save events
        for event in events:
            self._unprocessed_events[event.id] = event

        # get articles grouped by domains
        groups = self._distribute_unprocessed_articles(articles)

        # push articles to source-related processors
        for source, group in groups.items():
            # create domain subject and pipeline if it doesn't exist
            if source not in self._source_processor_subjects:
                subject = rx.subject.Subject()
                self._source_processor_subjects[source] = subject
                # print(
                #     f"Created new domain for source {source}; current subjects: {len(self._source_processor_subjects)}"
                # )

                subject.pipe(
                    # trottle - add delay after each item and merge with no concurrency
                    rxop.map(lambda a: rx.of(a).pipe(rxop.delay(0.5))),
                    rxop.merge(max_concurrent=1),
                    rxop.flat_map(
                        lambda a: rx.from_future(
                            self._aio_loop.create_task(self._parse_article(a))
                        )
                    ),
                ).subscribe(self._article_output.on_next, scheduler=self._scheduler)
            else:
                subject = self._source_processor_subjects[source]

            # push articles to domain processor
            for article in group:
                subject.on_next(article)

    def finalize_article(self, ap: _NewsArticleProcItem):
        """
        Receives processed articles from article source subjects.
        Groups articles by event and saves them to internal storage.
        Sends events with articles to output subject when all related articles are parsed.
        """
        evp: _NewsEventProcItem = self._unprocessed_events[ap.event_id]
        evp.processed_articles += 1

        if ap.article is not None:
            evp.articles.append(ap)
            self.processed_articles += 1
        else:
            self.skipped_articles += 1

        # fabricate Event object if all related articles are done being parsed
        if evp.processed_articles == evp.expected_article_count:
            # check if any articles got downloaded
            if len(evp.articles) > 0:
                # add skipped articles
                self.skipped_articles += evp.expected_article_count - len(evp.articles)

                # pick primary article to label the event
                primary_articles = [a.article for a in evp.articles if a.is_primary]

                # if it didn't download, pick another article
                if len(primary_articles) > 0:
                    primary_article = primary_articles[0]
                else:
                    primary_article = evp.articles[0]

                event = NewsEvent(
                    hash_sha256=evp.hash_sha256,
                    topic=evp.feed_request.topic,
                    locale=evp.feed_request.locale,
                    title=primary_article.title,
                    articles=list(map(lambda a: a.article, evp.articles)),
                )
                self._event_output.on_next(event)

            # delete now completely built event
            del self._unprocessed_events[ap.event_id]

            # complete parsing if no events are left
            if len(self._unprocessed_events) == 0:
                self._event_output.on_completed()

    def _generate_event_id(self):
        self._last_event_id += 1
        return self._last_event_id
