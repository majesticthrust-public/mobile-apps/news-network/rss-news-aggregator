import newspaper
from aggregator.common.aiohttp_client_user import AIOHttpClientUser


class AIONewspaper(AIOHttpClientUser):
    get_article_total = 0
    get_article_complete = 0
    get_article_incomplete = 0
    get_article_successful = 0
    get_article_errors = 0

    async def get_article(self, url: str, language: str = None) -> newspaper.Article:
        """
        Retuns newspaper.Article if successful, else None.
        """
        self.get_article_total += 1
        self.get_article_incomplete += 1
        async with self._aiohttp_session.get(url) as response:
            if response.status != 200:
                self.get_article_incomplete -= 1
                self.get_article_complete += 1
                self.get_article_errors += 1
                # TODO change to custom exception
                raise Exception(
                    f"Unsuccessful request; status {response.status}, message: '{await response.text()}'"
                )

            content = await response.read()
            article = newspaper.Article(url, language)
            article.set_html(content)
            article.parse()

            self.get_article_incomplete -= 1
            self.get_article_complete += 1
            self.get_article_successful += 1
            return article
        # try:

        # except Exception as e:
        #     self.get_article_errors += 1
        #     self.get_article_complete += 1
        #     self.get_article_incomplete -= 1
        #     print(f"Caught exception while getting article: {e}")
