from typing import NamedTuple
from aggregator.model.news import NewsEvent, NewsArticle
import hashlib
import feedparser
import newspaper


google_news_topics = [
    "TOP",  # special topic, it's not passed as a parameter in url
    "WORLD",
    "NATION",
    "BUSINESS",
    "TECHNOLOGY",
    "ENTERTAINMENT",
    "SPORTS",
    "SCIENCE",
    "HEALTH",
]


class UnprocessedArticle(NamedTuple):
    title: str
    url: str
    source: str
    locale: str
    language: str
    country: str
    topic: str


def get_feed_events(locale: str, topic: str):
    import bs4

    language, country = locale.split("-")

    if topic == "TOP":
        url = f"https://news.google.com/news/rss?hl={language}&gl={country}"
    elif topic in google_news_topics:
        url = (
            "https://news.google.com/news/rss"
            f"/headlines/section/topic/{topic}"
            f"?hl={language}&gl={country}"
        )
    else:
        raise Exception("Invalid topic {}".format(topic))

    print(f"Getting feed {locale} {topic}")

    feed = feedparser.parse(url)

    events = []
    for entry in feed.entries:
        event_hash_m = hashlib.sha256()
        event_hash_m.update(entry.title.encode())
        event = NewsEvent(
            topic=topic,
            locale=locale,
            title=entry.title,
            hash_sha256=event_hash_m.hexdigest(),
            articles=[],
        )
        events.append(event)

        soup = bs4.BeautifulSoup(entry.summary, features="lxml")
        # print(entry.summary)
        # print(soup.prettify())

        ol = soup.find("ol")
        items = []
        if ol is None:
            # there is no list if description contains only one link
            items.append(soup)
        else:
            items.extend(soup.ol.find_all("li", recursive=False))

        # print(f"{entry.title}, {max(len(items) - 1, 1)} links")

        for item in items:
            try:
                title = item.a.string
                url = item.a.get("href")
                source = item.font.string
                article = UnprocessedArticle(
                    title=title,
                    url=url,
                    source=source,
                    locale=locale,
                    language=language,
                    country=country,
                    topic=topic,
                )
                event.articles.append(article)
            except:
                continue

    return events


def process_event(event: NewsEvent):
    unprocessed_articles = event.articles
    event.articles = []
    for unprocessed_article in unprocessed_articles:
        article = _process_article(unprocessed_article)
        event.articles.append(article)

        # TODO advance from MVP
        # this break ensures that each event would have a single article
        # multiple articles per event require async downloading and parsing
        # and also it requires a clear reason to have multiple articles per event
        break

    return event


def _process_article(unprocessed_article: UnprocessedArticle):
    print(f"Article '{unprocessed_article.title}'")

    article_m = hashlib.sha256()
    article_m.update(unprocessed_article.title.encode())

    article = NewsArticle(
        title=unprocessed_article.title,
        text=None,
        authors=None,
        top_image=None,
        images=None,
        videos=None,
        publish_date=None,
        source_url=unprocessed_article.url,
        source_name=unprocessed_article.source,
        hash_sha256=article_m.hexdigest(),
    )

    try:
        nsar = newspaper.Article(
            unprocessed_article.url, language=unprocessed_article.language
        )

        nsar.download()
        nsar.parse()

        article_m.update(nsar.text.encode())

        article.text = nsar.text
        article.authors = list(nsar.authors)
        article.top_image = nsar.top_image
        article.images = list(nsar.images)
        article.videos = list(nsar.movies)
        article.publish_date = nsar.publish_date
    except Exception as e:
        print(f"An exception occured while getting article: {e}")
    finally:
        article.hash_sha256 = article_m.hexdigest()

    return article
