from typing import List, Dict, Tuple, Any, NamedTuple
import dataclasses as ds
import hashlib
import asyncio
import datetime as dt
import rx
import rx.operators as rxop
from rx.scheduler.eventloop import AsyncIOScheduler
from aggregator.feed_aggregator.aio_feedparser import AIOFeedparser
from aggregator.feed_aggregator.aio_newspaper import AIONewspaper
from aggregator.model.news import NewsArticle, NewsEvent


class _FeedRequest(NamedTuple):
    """
    Attributes:
        locale - language and country code pair (`en-US`),\
                ISO 639-1 + ISO 3166 alpha 2
        language - ISO 639-1 language code
        country - ISO 3166 alpha 2 country code
        topic - one of topic accepted by Google News; topic == TOP\
            equivalent to watching top news without topic
    """

    locale: str
    language: str
    country: str
    topic: str


@ds.dataclass
class _NewsArticleProcItem:
    """
    News Article for use in the internal processing pipeline
    """

    event_id: int
    url: str
    source: str
    feed_request: _FeedRequest
    # determines whether the article will be used to name the event
    is_primary: bool = False
    # None during processing, Article after parsing
    article: NewsArticle = None


@ds.dataclass
class _NewsEventProcItem:
    """
    News Event for use in the internal processing pipeline
    """

    id: int
    hash_sha256: str
    feed_request: _FeedRequest
    expected_article_count: int
    processed_articles: int = 0
    articles: List[_NewsArticleProcItem] = ds.field(default_factory=list)


google_news_topics = [
    "TOP",  # special topic, it's not passed as a parameter in url
    "WORLD",
    "NATION",
    "BUSINESS",
    "TECHNOLOGY",
    "ENTERTAINMENT",
    "SPORTS",
    "SCIENCE",
    "HEALTH",
]


class NewsGatherer:
    """
    Collects news from Google RSS Feeds for a given locale and topic.
    Asynchronous.

    The processing pipeline goes like this:
    1. User requests a few feeds by topic and locale
    2. Each request gets converted to a feed pulled from Google News
    3. Each feed gets broken down into Events - a groups of news Articles;\
        each event is saved in internal storage
    4. Each Event gets broken down into Articles;
    5. Each article is passed to it's respective domain pipeline; they are\
        parsed into text and links
    6. Parsing results are joined with the respective Events; Events are\
        emitted upon completion
    """

    processed_articles: int
    skipped_articles: int

    _aio_loop: asyncio.AbstractEventLoop
    _scheduler: AsyncIOScheduler
    _is_running: bool

    _feed_queue: List[_FeedRequest]
    _feed_request: rx.subject.ReplaySubject

    _event_output: rx.Observable

    _feedparser: AIOFeedparser
    _newspaper: AIONewspaper

    def __init__(self, loop: asyncio.AbstractEventLoop):
        self._aio_loop = loop
        self._scheduler = AsyncIOScheduler(loop)

        self.processed_articles = 0
        self.skipped_articles = 0

        self._feed_request = rx.subject.ReplaySubject(1)
        self._feed_queue = []
        self._is_running = False

        self._feedparser = AIOFeedparser()
        self._newspaper = AIONewspaper()

        self._event_output = self._feed_request.pipe(
            # map feed requests to feeds
            # TODO handle potential error when getting feed
            rxop.flat_map(
                lambda rq: rx.from_future(
                    self._aio_loop.create_task(self._get_feed(rq))
                )
            ),
            rxop.flat_map(
                lambda feed_rq: rx.from_future(
                    self._aio_loop.create_task(
                        self._breakdown_feed(feed_rq[0], feed_rq[1])
                    )
                ),
            ),
            rxop.share(),
        )

        # debug log
        self._event_output.subscribe(
            lambda e: print(f"Aggregator output event: {e.title}")
        )

    def queue_feed(self, locale: str, topic: str):
        """
        Queue a feed to parse. Does not start parsing immideately.
        Call "run" to start parsing. After launching, queue_feed will raise
        an exception.
        """
        if self._is_running:
            # TODO change to custom excetion
            raise Exception("Can't queue feed: parser is running!")

        language, country = locale.split("-")
        request = _FeedRequest(locale, language, country, topic)
        self._feed_queue.append(request)

    def start(self):
        """
        Start parsing.
        """
        if self._is_running:
            # TODO change to custom excetion
            raise Exception("Can't run: parser is already running!")

        self._is_running = True

        for request in self._feed_queue:
            self._feed_request.on_next(request)

        self._feed_request.on_completed()

    def events(self) -> rx.Observable:
        """
        Returns an observable that emits parsed news events.
        Completes when all events are parsed successfully.
        """
        return self._event_output

    async def _get_feed(self, request: _FeedRequest) -> Tuple[_FeedRequest, Any]:
        """
        Arguments:
            language - ISO 639-1 language code
            country - ISO 3166 alpha 2 country code
            topic - one of topic accepted by Google News; topic == TOP\
                equivalent to watching top news without topic
        """

        _, language, country, topic = request

        if topic == "TOP":
            url = f"https://news.google.com/news/rss?hl={language}&gl={country}"
        elif topic in google_news_topics:
            url = (
                "https://news.google.com/news/rss"
                f"/headlines/section/topic/{topic}"
                f"?hl={language}&gl={country}"
            )
        else:
            raise Exception("Invalid topic {}".format(topic))

        feed = await self._feedparser.parse(url)

        return feed, request

    async def _breakdown_feed(
        self, feed, request: _FeedRequest
    ) -> Tuple[List[_NewsEventProcItem], List[_NewsArticleProcItem]]:
        """
        Breakdowns the feed into Events and Articles.
        Publishes parsed events to the output subject.
        """
        output_events = []

        for entry in feed.entries:

            event_hash = hashlib.sha256()
            event_hash.update(entry.title.encode())

            article_hash = event_hash

            d = entry.published_parsed

            article = NewsArticle(
                title=entry.title,
                text="",
                authors=[],
                top_image="",
                images=[],
                videos=[],
                publish_date=dt.datetime(
                    year=d.tm_year,
                    month=d.tm_mon,
                    day=d.tm_mday,
                    hour=d.tm_hour,
                    minute=d.tm_min,
                    second=d.tm_sec,
                ),
                source_url=entry.link,
                source_name=entry.source.title,
                hash_sha256=article_hash.hexdigest(),
            )

            try:
                nsar = await self._newspaper.get_article(entry.link, request.language)
                article.text = nsar.text
                article.authors = nsar.authors
                article.top_image = nsar.top_image
                article.images = nsar.images
                article.videos = nsar.movies
            except Exception as e:
                print(f"Error while parsing article: {e}")

            event = NewsEvent(
                topic=request.topic,
                locale=request.locale,
                title=entry.title,
                hash_sha256=event_hash.hexdigest(),
                articles=[article],
            )

            output_events.append(event)

        # return rx.from_iterable(output_events)
        return output_events

        # publish parsed event
        # self._event_output.on_next(event)
